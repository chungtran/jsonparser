/******************************************************************************
 * Copyright (c) 2015, Belkin Inc. All rights reserved.
 *
 ******************************************************************************/

#ifndef _JSON_OBJECT_H_
#define _JSON_OBJECT_H_

#include <map>
#include <string.h>
#include <stdlib.h>
#include <iostream>

 using namespace std;

	class JsonObject
	{
	public:
		// JsonObject(string const& SenderID, multimap<string,string> const& RcptID,
		// 			string const& TypeID, multimap<string,string> const& Data, string const& flags);
		~JsonObject();

		const string& getSenderID() const;

		const multimap<string,string>& getRcptID() const;

		const string& getTypeID() const;

		const multimap<string,string>& getData() const;

		const string& getFlags() const;

		void set_SenderID(string);

		void set_TypeID(string);

		void set_flags(string);

		void set_data(multimap<string, string>&);

		void set_RcptID(multimap<string, string>&);

		string createJsonMessage(JsonObject object);
	
	private:
		
		string m_SenderID;

		multimap<string,string> m_RcptID;

		string m_TypeID;

		multimap<string,string> m_Data;

		string m_flags;
	};

#endif /* _JSON_OBJECT_H_ */
