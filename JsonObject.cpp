/******************************************************************************
 * Copyright (c) 2015, Belkin Inc. All rights reserved.
 *
 ******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <iostream>
#include <vector>
#include <map>
#include "JsonObject.h"

using namespace std;

	// JsonObject::JsonObject(string const& SenderID, multimap<string,string> const& RcptID,
	// 				string const& TypeID, multimap<string,string> const& Data, string const& flags) : 
	// 				m_SenderID(SenderID), m_RcptID(RcptID), m_TypeID(TypeID), m_Data(Data), m_flags(flags)
	// {
	// }

JsonObject::~JsonObject()
{
}

const string& JsonObject::getSenderID() const
{
	return m_SenderID;
}

const multimap<string,string>& JsonObject::getRcptID() const
{
	return m_RcptID;
}

const string& JsonObject::getTypeID() const
{
	return m_TypeID;
}

const multimap<string,string>& JsonObject::getData() const
{
	return m_Data;
}

const string& JsonObject::getFlags() const
{
	return m_flags;
}

/* Create Json Message Fuction 
   string JsonObject::createJsonMessage(JsonObject object)
   Return : Json Message String
*/

string JsonObject::createJsonMessage(JsonObject object)
{

  string s;

  json_t *root = json_object();
  json_t *json_arr_RcptID = json_array();
  json_t *json_arr_Data = json_array();
  json_t *data = json_object();
  json_t *RcptID= json_object();

  multimap<string,string> temp_RcptID, temp_Data;
  temp_Data = object.getData();
  temp_RcptID = object.getRcptID();
  
  
  if(!(temp_Data.empty()))
  {
      for (multimap<string,string>::iterator iter=temp_Data.begin(); iter!=temp_Data.end(); ++iter)
      {
          json_object_set_new( data,((*iter).first).c_str(), json_string(((*iter).second).c_str()));
          //cout << (*it).first << " = " << (*it).second << '\n';
      }
  }

  json_array_append_new( json_arr_Data, data );

  json_object_set_new( root, "data", json_arr_Data);

  if(!(temp_RcptID.empty()))
  {
      for (multimap<string,string>::iterator iter=temp_RcptID.begin(); iter!=temp_RcptID.end(); ++iter)
      {
          json_object_set_new( RcptID,((*iter).first).c_str(), json_string(((*iter).second).c_str()));
          // cout << (*it).first << " = " << (*it).second << '\n';
      }
  }

  json_array_append_new( json_arr_RcptID, RcptID );

  json_object_set_new( root, "RcptID", json_arr_RcptID);

  if(((object.getSenderID()).c_str()))
  {
    json_object_set_new( root, "SenderID", json_string(object.getSenderID().c_str()));
  }
 
  if(((object.getTypeID()).c_str()))
  {
    son_object_set_new( root, "TypeID", json_string(object.getTypeID().c_str()));
  }

  if(((object.getFlags()).c_str()))
  {
    json_object_set_new( root, "flags", json_string(object.getFlags().c_str()));
  }

  return s = json_dumps(root, JSON_INDENT(2));

}

// Set functions

void JsonObject::set_SenderID(string sender_id)
{
 m_SenderID = sender_id;
}
void JsonObject::set_TypeID(string type_id)
{
 m_TypeID = type_id;
}
void JsonObject::set_flags(string flag)
{
 m_flags = flag;
}
void JsonObject::set_data(multimap<string, string> &mes_data)
{
 m_Data = mes_data;
}
void JsonObject::set_RcptID(multimap<string, string> &mes_RcptID)
{
 m_RcptID = mes_RcptID;
}

  