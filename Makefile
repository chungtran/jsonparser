.PHONY: all, clean, main

CXX :=g++

INCLUDE_FLAGS := -I/home/chungtran/Jansson-source/Dev/jansson-2.7/build/include

CFLAGS := -c $(INCLUDE_FLAGS)

LDFLAGS := -L/home/chungtran/Jansson-source/Dev/jansson-2.7/build/lib

LDLIBS := -ljansson
 
SOURCES_0 := JsonCreation.cpp

SOURCES_1 := JsonObject.cpp 

OBJECTS_0 := $(SOURCES_0:.cpp=.o)

OBJECTS_1 := $(SOURCES_1:.cpp=.o)

EXCUTABLE := main

all: $(SOURCES_0) $(EXCUTABLE)

$(EXCUTABLE): $(OBJECTS_0) $(OBJECTS_1)
	$(CXX) -o $@ $(OBJECTS_1) $(OBJECTS_0) $(LDFLAGS) $(LDLIBS)
	echo "Finish !"
.cpp.o:
	$(CXX) $(CFLAGS) $(SOURCES_1)
	$(CXX) $(CFLAGS) $< -o $@
clean:
	echo "Cleaning source ...."
	rm -rf main JsonCreation.o JsonObject.o

	 
